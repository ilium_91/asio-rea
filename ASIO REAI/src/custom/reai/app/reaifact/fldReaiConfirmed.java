package custom.reai.app.reaifact;

import psdi.mbo.MboRemote;
import psdi.mbo.MboValue;
import psdi.mbo.MboValueAdapter;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class fldReaiConfirmed extends MboValueAdapter {

    public fldReaiConfirmed(MboValue mbv) {
        super(mbv);
    }

    public void validate()
            throws MXException, RemoteException {
        super.validate();
        MboRemote recordValue = this.getMboValue().getMbo();
        Boolean reaiconfirmed = getMboValue().getBoolean();
        Boolean reaistudy = recordValue.getBoolean("REAISTUDY");
        if  (reaiconfirmed && reaistudy) {
            throw new MXApplicationException("REAI", "factCategory");
        }
        else return;
    }
}