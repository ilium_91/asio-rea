package custom.reai.app.reaifact;

import psdi.mbo.MboRemote;
import psdi.mbo.MboValue;
import psdi.mbo.MboValueAdapter;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class fldReaiPositive extends MboValueAdapter {

    public fldReaiPositive(MboValue mbv) {
        super(mbv);
    }

    public void validate()
            throws MXException, RemoteException {
        super.validate();
        MboRemote recordValue = this.getMboValue().getMbo();
        Boolean reaipositive = getMboValue().getBoolean();
        Boolean reaistudy = recordValue.getBoolean("REAISTUDY");
        if (reaipositive && reaistudy) {
                 throw new MXApplicationException("REAI", "factCategory");
             }
        else return;
    }
}
