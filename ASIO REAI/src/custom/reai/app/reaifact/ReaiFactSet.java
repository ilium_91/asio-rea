package custom.reai.app.reaifact;

import psdi.mbo.Mbo;
import psdi.mbo.MboServerInterface;
import psdi.mbo.MboSet;
import psdi.mbo.custapp.CustomMboSet;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class ReaiFactSet extends CustomMboSet implements ReaiFactSetRemote {

    public ReaiFactSet(MboServerInterface ms) throws RemoteException {
        super(ms);
    }

    protected Mbo getMboInstance(MboSet ms) throws MXException, RemoteException {
        return new ReaiFact(ms);
    }
}