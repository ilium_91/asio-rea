package custom.reai.app.reaifact;

import psdi.mbo.MboRemote;
import psdi.mbo.MboValue;
import psdi.mbo.MboValueAdapter;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class fldReaiStudy extends MboValueAdapter {

    public fldReaiStudy(MboValue mbv) {
        super(mbv);
    }

    public void validate()
            throws MXException, RemoteException {
        super.validate();
        MboRemote recordValue = this.getMboValue().getMbo();
        Boolean reaiconfirmed = recordValue.getBoolean("REAICONFIRMED");
        Boolean reaipositive = recordValue.getBoolean("REAIPOSITIVE");
        Boolean reaistudy = getMboValue().getBoolean();
        if (reaiconfirmed && reaistudy) {
            throw new MXApplicationException("REAI", "factCategory");
        }
        else if (reaipositive && reaistudy) {
            throw new MXApplicationException("REAI", "factCategory");
        }
        else return;
    }
}
