package custom.reai.app.reaifact;

import psdi.mbo.MboRemote;
import psdi.mbo.MboSet;
import psdi.mbo.MboSetRemote;
import psdi.mbo.custapp.CustomMbo;
import psdi.server.MXServer;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;
import java.util.Date;

public class ReaiFact extends CustomMbo implements ReaiFactRemote {
    boolean isModified = false;

    public ReaiFact(MboSet ms) throws RemoteException {
        super(ms);
    }

    @Override
    public void save() throws MXException, RemoteException {
        if (this.getString("CLASSSTRUCTUREID")!= null && !this.getString("CLASSSTRUCTUREID").equals("")){
            if (this.getMboSet("CLASSSTRUCTURE").count() > 0) {
                MboRemote cl = this.getMboSet("CLASSSTRUCTURE").getMbo(0);
                this.setValue("REAITOPICDESC", cl.getString("DESCRIPTION"), 11L);
            }
        }
        if (!this.getBoolean("REAIISMOBILE")){
            if (this.isNull("REAICREATEDATE") || this.getDate("REAICREATEDATE") == null) {
                Date currentDate = MXServer.getMXServer().getDate();
                this.setValue("REAICREATEDATE", currentDate, 11L);
            }
            boolean confirmed = this.getBoolean("REAICONFIRMED");
            boolean positive = this.getBoolean("REAIPOSITIVE");
            boolean study = this.getBoolean("REAISTUDY");
            if (!confirmed && !positive &&!study) {
                throw new MXApplicationException("REAI", "factCategory");
            }
            MboSetRemote routeSet = this.getMboSet("REAIROUTE");
            if (routeSet.count()> 0) {
                String routeCode = routeSet.getMbo(0).getString("REAICODE");
                String classstructureid = this.getString("CLASSSTRUCTUREID");
                String reaifactid = this.getString("REAIFACTID");
                if (!classstructureid.equals("") && classstructureid != null) {
                    if (confirmed){
                        this.setValue("REAICODE", routeCode + "1" + classstructureid + reaifactid, 11L);
                    }
                    else if (!confirmed && positive) {
                        this.setValue("REAICODE", routeCode + "2" + classstructureid + reaifactid, 11L);
                    }
                    else if (study)
                    {
                        this.setValue("REAICODE", routeCode + "3" + classstructureid + reaifactid, 11L);
                    }
                }
            }
        }
        super.save();
    }

    @Override
    public void add() throws MXException, RemoteException {
        Date currentDate = MXServer.getMXServer().getDate();
        this.setValue("REAICHANGEBY", this.getUserInfo().getPersonId(), 11L);
        this.setValue("REAICHANGEDATE", currentDate, 11L);
    }

    @Override
    public void modify() throws MXException, RemoteException {
        if (!this.isModified) {
            this.isModified = true;
            this.setValue("REAICHANGEBY", this.getUserInfo().getPersonId(), 11L);
            this.setValue("REAICHANGEDATE", MXServer.getMXServer().getDate(), 11L);
        }
    }

    public ReaiFactRemote duplicateWithPhoto()
            throws MXException, RemoteException
    {
        ReaiFactRemote newFactRemote = (ReaiFactRemote) this.copy();
        MboSetRemote myTicketDocs = getMboSet("DOCLINKS");
        myTicketDocs.copy(newFactRemote.getMboSet("DOCLINKS"));
        MboSetRemote myParentRoute = getMboSet("REAIROUTE");
        newFactRemote.setValue("REAISTATUS", "ЧЕРНОВИК", 11L);
        return newFactRemote;
    }

    public ReaiFactRemote duplicateNoPhoto()
            throws MXException, RemoteException
    {
        ReaiFactRemote newFactRemote = (ReaiFactRemote) this.copy();
        newFactRemote.setValue("REAISTATUS","ЧЕРНОВИК", 11L);
        return newFactRemote;
    }


}
