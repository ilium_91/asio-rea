package custom.reai.app.reaifact;

import psdi.mbo.custapp.CustomMboRemote;
import psdi.util.MXException;

import java.rmi.RemoteException;

public interface ReaiFactRemote extends CustomMboRemote {
    ReaiFactRemote duplicateWithPhoto()
            throws MXException, RemoteException;
    ReaiFactRemote duplicateNoPhoto()
            throws MXException, RemoteException;

}