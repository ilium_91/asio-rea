package custom.reai.app.reaiinspection;

import psdi.mbo.Mbo;
import psdi.mbo.MboServerInterface;
import psdi.mbo.MboSet;
import psdi.mbo.custapp.CustomMboSet;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class ReaiInspectionSet extends CustomMboSet implements ReaiInspectionSetRemote {

    public ReaiInspectionSet(MboServerInterface ms) throws RemoteException {
        super(ms);
    }

    protected Mbo getMboInstance(MboSet ms) throws MXException, RemoteException
    {
        return new ReaiInspection(ms);
    }

}