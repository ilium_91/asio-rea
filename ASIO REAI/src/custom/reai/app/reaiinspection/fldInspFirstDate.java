package custom.reai.app.reaiinspection;

import psdi.mbo.MboValue;
import psdi.mbo.MboValueAdapter;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;
import java.util.Date;

public class fldInspFirstDate
        extends MboValueAdapter
{
    public fldInspFirstDate(MboValue mbv)
    {
        super(mbv);
    }

    public void validate()
            throws MXException, RemoteException
    {
        super.validate();
        MboValue myValue = getMboValue();
        if (myValue.isNull()) {
            return;
        }
        Date myDate = myValue.getDate();
        Date lastDate = this.getMboValue().getMbo().getDate("REAIINSPECTIONLASTDATE");
        if (lastDate == null) {
            return;
        }
        if (myDate.after(lastDate)) {
            throw new MXApplicationException("REAI", "inspFDateOutRange");
        }
    }
}