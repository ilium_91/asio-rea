package custom.reai.app.reaiinspection;

import psdi.mbo.MboSet;
import psdi.mbo.MboSetRemote;
import psdi.mbo.custapp.CustomMbo;
import psdi.server.MXServer;
import psdi.util.MXException;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;

public class ReaiInspection extends CustomMbo implements ReaiInspectionRemote{

    boolean isModified = false;

    public ReaiInspection(MboSet ms) throws RemoteException {
        super(ms);
    }

    @Override
    protected void save() throws MXException, RemoteException {
        Calendar cal = Calendar.getInstance();
        Date inspLastDate = this.getDate("REAIINSPECTIONLASTDATE");
        if (inspLastDate != null) {
            cal.setTime(inspLastDate);
            String year = String.valueOf(cal.get(Calendar.YEAR));
            String month = String.valueOf(cal.get(Calendar.MONTH)+1);
            String aes = this.getString("REAIAES");
            String checkCode = this.getString("REAICODE");
            if (checkCode.equals("")) {
                if (!aes.equals("")) {
                    MboSetRemote inspCountSet = this.getMboSet("REAIINSPONMONTH");
                    int inspCount = 1;
                    if (inspCountSet.count() == 0)
                    {
                        this.setValue("REAIINSPONMONTH", 11L);
                    }
                    else {
                        for (int i = 0; i < inspCountSet.count(); i++) {
                            if (inspCountSet.getMbo(i).getInt("REAIINSPONMONTH") > inspCount) {
                                inspCount = inspCountSet.getMbo(i).getInt("REAIINSPONMONTH");
                            }
                        }
                        this.setValue("REAIINSPONMONTH", ++inspCount, 11L);
                    }
                    String code = aes + year + month + "_" + String.valueOf(inspCount);
                    this.setValue("REAICODE", code, 11L);
                }

            }
        }
        super.save();
    }


    @Override
    public void add() throws MXException, RemoteException {
        this.setValue("REAICHANGEDATE", MXServer.getMXServer().getDate(), 11L);
        this.setValue("REAICHANGEBY", this.getUserInfo().getPersonId(), 11L);
    }

    @Override
    public void modify() throws MXException, RemoteException {
        if (!this.isModified) {
            this.isModified = true;
            this.setValue("REAICHANGEDATE", MXServer.getMXServer().getDate(), 11L);
            this.setValue("REAICHANGEBY", this.getUserInfo().getPersonId(), 11L);
        }
    }
}
