package custom.reai.app.reaiinspection;


import psdi.mbo.MboValue;
import psdi.mbo.MboValueAdapter;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;
import java.util.Date;

public class fldInspLastDate
        extends MboValueAdapter
{
    public fldInspLastDate(MboValue mbv)
    {
        super(mbv);
    }

    public void validate()
            throws MXException, RemoteException
    {
        super.validate();
        MboValue myValue = getMboValue();
        if (myValue.isNull()) {
            return;
        }
        Date myDate = myValue.getDate();
        Date firstDate = this.getMboValue().getMbo().getDate("REAIINSPECTIONFIRSTDATE");
        if (firstDate == null) {
            return;
        }
        if (myDate.before(firstDate)) {
            throw new MXApplicationException("REAI", "inspLDateOutRange");
        }
    }
}