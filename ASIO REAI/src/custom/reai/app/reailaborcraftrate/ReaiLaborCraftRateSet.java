package custom.reai.app.reailaborcraftrate;

import psdi.mbo.Mbo;
import psdi.mbo.MboServerInterface;
import psdi.mbo.MboSet;
import psdi.mbo.custapp.CustomMboSet;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class ReaiLaborCraftRateSet extends CustomMboSet implements ReaiLaborCraftRateSetRemote {

    public ReaiLaborCraftRateSet(MboServerInterface ms) throws RemoteException {
        super(ms);
    }

    protected Mbo getMboInstance(MboSet ms) throws MXException, RemoteException
    {
        return new ReaiLaborCraftRate(ms);
    }
}
