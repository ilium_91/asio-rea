package custom.reai.app.reailaborcraftrate;


import psdi.app.labor.LaborCraftRate;
import psdi.mbo.MAXTableDomain;
import psdi.mbo.MboValue;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class fldReaiDefaultCraft
        extends MAXTableDomain
{
    public fldReaiDefaultCraft(MboValue mbv)
            throws MXException
    {
        super(mbv);
    }

    public void validate() {}

    public void action()
            throws MXException, RemoteException
    {
        ReaiLaborCraftRate rate = (ReaiLaborCraftRate)getMboValue().getMbo();
        rate.makeOtherNotDefault();
        rate.setValue("defaultcraft", true, 11L);
        rate.setFieldFlag("defaultcraft", 7L, true);
    }



}
