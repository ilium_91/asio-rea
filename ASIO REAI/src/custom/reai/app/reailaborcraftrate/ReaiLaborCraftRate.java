package custom.reai.app.reailaborcraftrate;

import psdi.mbo.MboRemote;
import psdi.mbo.MboSet;
import psdi.mbo.MboSetRemote;
import psdi.mbo.custapp.CustomMbo;
import psdi.txn.MXTransaction;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class ReaiLaborCraftRate extends CustomMbo implements ReaiLaborCraftRateRemote {
    public ReaiLaborCraftRate(MboSet ms) throws RemoteException {
        super(ms);
    }

    public void makeOtherNotDefault() throws MXException, RemoteException {
        MboSetRemote thisMboSet = this.getThisMboSet();
        MboRemote mbo = null;
        int i = 0;
        boolean done = false;
        for(mbo = thisMboSet.getMbo(i); mbo != null && !done; mbo = thisMboSet.getMbo(i)) {
            if (mbo != this && mbo.getBoolean("REAIDEFAULTCRAFT") && mbo.getString("REAILABORCODE").equals(this.getString("REAILABORCODE")) && mbo.getString("orgid").equals(this.getString("orgid"))) {
                mbo.setValue("REAIDEFAULTCRAFT", false, 11L);
                mbo.setFieldFlag("REAIDEFAULTCRAFT", 7L, false);
                done = true;
            }

            ++i;
        }
        if (!done) {
            MboSetRemote rates = null;
            MXTransaction txn = this.getMXTransaction();
            boolean integration = false;
            if (txn != null && txn.getBoolean("integration")) {
                integration = true;
            }
            if (!integration && this.getUserInfo().isInteractive()) {
                rates = this.getMboSet("$LABORCRAFTRATE", "REAILABORCRAFTRATE", "reailaborcode=:reailaborcode and orgid=:orgid");
            } else {
                rates = ((MboSet)thisMboSet).getSharedMboSet("REAILABORCRAFTRATE", "reailaborcode=:reailaborcode and orgid=:orgid");
            }
            MboRemote rate = null;
            i = 0;
            for(rate = rates.getMbo(i); rate != null; rate = rates.getMbo(i)) {
                if (rate != this && rate.getBoolean("REAIDEFAULTCRAFT")) {
                    rate.setValue("REAIDEFAULTCRAFT", false, 11L);
                    rate.setFieldFlag("REAIDEFAULTCRAFT", 7L, false);
                    break;
                }
                ++i;
            }
        }

    }
}