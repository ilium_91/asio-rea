package custom.reai.app.reailaborcraftrate;

import psdi.mbo.custapp.CustomMboRemote;
import psdi.util.MXException;

import java.rmi.RemoteException;

public interface ReaiLaborCraftRateRemote extends CustomMboRemote {
    public abstract void makeOtherNotDefault()
            throws MXException, RemoteException;

}
