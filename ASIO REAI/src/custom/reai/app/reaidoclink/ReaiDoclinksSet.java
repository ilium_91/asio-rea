package custom.reai.app.reaidoclink;

import psdi.app.doclink.DoclinksSet;
import psdi.mbo.Mbo;
import psdi.mbo.MboServerInterface;
import psdi.mbo.MboSet;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class ReaiDoclinksSet extends DoclinksSet implements ReaiDoclinksSetRemote {

    public ReaiDoclinksSet(MboServerInterface ms) throws MXException, RemoteException {
        super(ms);
    }

    protected Mbo getMboInstance(MboSet ms) throws MXException, RemoteException {
        return new ReaiDoclinks(ms);
    }
}
