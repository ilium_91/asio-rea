package custom.reai.app.reaidoclink;

import psdi.app.doclink.Doclinks;
import psdi.mbo.MboSet;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class ReaiDoclinks extends Doclinks {

    public ReaiDoclinks(MboSet ms) throws MXException, RemoteException {
        super(ms);
    }
}

