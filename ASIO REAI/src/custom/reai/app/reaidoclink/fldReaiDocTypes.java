package custom.reai.app.reaidoclink;

import psdi.mbo.*;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class fldReaiDocTypes
        extends MAXTableDomain
{
    public fldReaiDocTypes(MboValue mbv)
            throws MXException, RemoteException {
        super(mbv); // mbv из класса родителя MAXTableDomain

        setRelationship("APPDOCTYPE", "doctype = :doctype and app = :app");// создаем взаимосвязь APPDOCTYPE, для записи вложений для приложений
        setListCriteria("(app = :app and :ownertable = 'REAIINSPECTION' and doctype = :reaicode) or (app = :app and :ownertable = 'REAIFACT' and :reaicode = doctype)" +
                "or (app = :app and :ownertable = 'REAIROUTE' and doctype = :reaicode)");// or(app = :app and :ownertable not in  ('REAIFACT', 'REAIINSPECTION')"); // Критерии листа, 3 варианта, для REAIINSPECTION, для REAIFACT, для других объектов

        setLookupKeyMapInOrder(new String[] { "doctype", "app" }, new String[] { "doctype", "app" }); // Сортируем массивы по типу документа и приложению
    }

    public void validate()   // метод утверждения
            throws MXException, RemoteException
    {
        if (getMboValue().isNull()) // если запись пустая, то ничего для нее не возвращаем
        {
            return;
        }
        MboRemote doclinks = getMboValue().getMbo(); // создаем переменную класса MboRemote doclinks, в которую заносим значение из значения считанной записи.
        if ((!doclinks.isNull("app")) && (!doclinks.getString("app").equalsIgnoreCase("COMMTMPLT"))) // если есть приложение, то в его папку отправляем
        {
            super.validate();
        }
        else // в другом случае, создаем переменную sqf Sql-формата
        {
            SqlFormat sqf = new SqlFormat(doclinks.getUserInfo(), "doctype = :1");
            sqf.setObject(1, "DOCTYPES", "DOCTYPE", doclinks.getString("doctype"));
            MboSetRemote docset = ((Mbo)doclinks).getMboSet("$doctype" + doclinks.getString("doctype"), "DOCTYPES", sqf.format());
            if (docset.isEmpty())
            {
                docset.close();
                Object[] params = { getMboValue().getColumnTitle(), getMboValue().getCurrentValue().toString() };
                throw new MXApplicationException("system", "notvalid", params);
            }
            docset.close();
        }
    }
}
