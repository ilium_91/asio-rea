package custom.reai.app.reairoute;

import psdi.mbo.MboSetRemote;
import psdi.mbo.custapp.CustomMboRemote;
import psdi.util.MXException;

import java.rmi.RemoteException;

public interface ReaiRouteRemote extends CustomMboRemote {
    public void copyComissionMembers(MboSetRemote CommissionSet)
            throws MXException, RemoteException;
}
