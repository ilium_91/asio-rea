package custom.reai.app.reairoute;

import psdi.mbo.Mbo;
import psdi.mbo.MboServerInterface;
import psdi.mbo.MboSet;
import psdi.mbo.custapp.CustomMboSet;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class ReaiRouteSet extends CustomMboSet implements ReaiRouteSetRemote {
    public ReaiRouteSet(MboServerInterface ms) throws RemoteException {
        super(ms);
    }
    protected Mbo getMboInstance(MboSet ms) throws MXException, RemoteException
    {
        return new ReaiRoute(ms);
    }
}