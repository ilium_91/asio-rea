package custom.reai.app.reairoute;

import psdi.mbo.MboRemote;
import psdi.mbo.MboValue;
import psdi.mbo.MboValueAdapter;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;

public class fldRouteFinishDate
        extends MboValueAdapter
{
    public fldRouteFinishDate(MboValue mbv)
    {
        super(mbv);
    }

    public void validate()
            throws MXException, RemoteException
    {
        super.validate();
        MboValue myValue = getMboValue();
        if (myValue.isNull()) {
            return;
        }
        Date myDate = myValue.getDate();
        Date startDate = this.getMboValue().getMbo().getDate("REAISTARTDATE");
        MboRemote insp = this.getMboValue().getMbo().getMboSet("REAIINSPECTION").getMbo(0);
        Date inspFirstDate = insp.getDate("REAIINSPECTIONFIRSTDATE");
        Date inspLastDate = insp.getDate("REAIINSPECTIONLASTDATE");
        Calendar instance = Calendar.getInstance();
        instance.setTime(inspLastDate);
        instance.add(Calendar.DAY_OF_MONTH, 1);
        Date newInspLastDate = instance.getTime();
        if (myDate.before(inspFirstDate) || myDate.after(newInspLastDate)) {
            throw new MXApplicationException("REAI", "routeFDateOutRangeI");
        }
        if (myDate.before(startDate)) {
            throw new MXApplicationException("REAI", "routeFDateOutRange");
        }
    }
}