package custom.reai.app.reairoute;

import psdi.mbo.MboRemote;
import psdi.mbo.MboValue;
import psdi.mbo.MboValueAdapter;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;
import java.util.Date;

public class fldRouteStartDate
        extends MboValueAdapter
{
    public fldRouteStartDate(MboValue mbv)
    {
        super(mbv);
    }

    public void validate()
            throws MXException, RemoteException
    {
        super.validate();
        MboValue myValue = getMboValue();
        if (myValue.isNull()) {
            return;
        }
        Date myDate = myValue.getDate();
        Date finishDate = this.getMboValue().getMbo().getDate("REAIFINISHDATE");
        MboRemote insp = this.getMboValue().getMbo().getMboSet("REAIINSPECTION").getMbo(0);
        Date inspFirstDate = insp.getDate("REAIINSPECTIONFIRSTDATE");
        Date inspLastDate = insp.getDate("REAIINSPECTIONLASTDATE");
        if (myDate.before(inspFirstDate) || myDate.after(inspLastDate)) {
            throw new MXApplicationException("REAI", "routeSDateOutRangeI");
        }
        if (myDate.after(finishDate)) {
            throw new MXApplicationException("REAI", "routeSDateOutRange");
        }
    }
}