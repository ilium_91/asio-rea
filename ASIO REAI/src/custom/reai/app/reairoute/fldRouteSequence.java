package custom.reai.app.reairoute;

import psdi.mbo.MboValue;
import psdi.mbo.MboValueAdapter;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;

public class fldRouteSequence
        extends MboValueAdapter
{
    public fldRouteSequence(MboValue mbv)
    {
        super(mbv);
    }

    public void validate()
            throws MXException, RemoteException
    {
        super.validate();
        MboValue myValue = getMboValue();
        if (myValue.isNull()) {
            return;
        }
        int myInt = myValue.getInt();
        if (myInt <= 0) {
            throw new MXApplicationException("REAI", "NotValidSequence");
        }
    }
}