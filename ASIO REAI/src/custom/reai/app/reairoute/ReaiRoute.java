package custom.reai.app.reairoute;

import psdi.mbo.MboRemote;
import psdi.mbo.MboSet;
import psdi.mbo.MboSetRemote;
import psdi.mbo.custapp.CustomMbo;
import psdi.server.MXServer;
import psdi.util.MXApplicationException;
import psdi.util.MXException;

import java.rmi.RemoteException;
import java.util.Vector;

public class ReaiRoute extends CustomMbo implements ReaiRouteRemote {
    boolean isModified = false;

    public ReaiRoute(MboSet ms) throws RemoteException {
        super(ms);
    }

    @Override
    protected void save() throws MXException, RemoteException {
        String inspCode = this.getMboSet("REAIINSPECTION").getMbo(0).getString("REAICODE");
        String routeSeq = this.getString("REAISEQUENCE");
        int routeSeqInt = this.getInt("REAISEQUENCE");
        String routeType = this.getString("REAIROUTETYPE");
        String routeNum = this.getString("REAIROUTENUM");
        MboSetRemote allRoutes = this.getMboSet("REAIINSPECTION").getMbo(0).getMboSet("REAIROUTE");
        int seqCount = allRoutes.count();
        boolean checkSeq = true;
        for (int i = 0; i < seqCount; i++) {
            MboRemote checkRoute = allRoutes.getMbo(i);
            String currSeq = checkRoute.getString("REAISEQUENCE");
            if (routeSeq.equals(currSeq) && !routeNum.equals(checkRoute.getString("REAIROUTENUM"))) {
                checkSeq = false;
                break;
            }
        }
        if (!checkSeq) {
            throw new MXApplicationException("REAI", "wrongseq");
        }
        else {
            if (routeSeqInt < 10) {
                this.setValue("REAICODE", inspCode+routeType+"0"+routeSeq, 11L);
                super.save();
            }
            else {
                this.setValue("REAICODE", inspCode+routeType+routeSeq, 11L);
                super.save();
            }
        }
    }

    @Override
    public void modify() throws MXException, RemoteException {
        if (!this.isModified) {
            this.isModified = true;
            this.setValue("REAICHANGEDATE", MXServer.getMXServer().getDate(), 11L);
            this.setValue("REAICHANGEBY", this.getUserInfo().getPersonId(), 11L);
        }
    }
/*
    @Override
    public void add() throws MXException, RemoteException {
        this.setValue("REAISTARTDATE", this.getMboSet("REAIINSPECTION").getMbo(0).getDate("REAIINSPECTIONFIRSTDATE"), 11L);
        this.setValue("REAIFINISHDATE", this.getMboSet("REAIINSPECTION").getMbo(0).getDate("REAIINSPECTIONLASTDATE"), 11L);
    }
*/
    public void copyComissionMembers(MboSetRemote CommissionSet)
            throws MXException, RemoteException {
        MboSetRemote ComissionMembersSet = this.getMboSet("REAICOMISSIONMEMBERS");
        Vector ComissionVector = CommissionSet.getSelection();
        if (ComissionVector.size() != 0) {
            int i = 0;
            int size = ComissionVector.size();
            for (MboRemote laborMbo = null; i < size; ++i) {
                if ((laborMbo = (MboRemote) ComissionVector.elementAt(i)) != null) {
                    int j = 0;
                    int k = 0;
                    int sizecom = ComissionMembersSet.count();
                    while (j < sizecom) {
                        if (ComissionMembersSet.getMbo(j).getString("REAILABORCODE").equals(laborMbo.getString("REAILABORCODE"))) {
                            k = 1;
                        }
                        j++;
                    }
                    if (k == 0) {
                        MboRemote ComissionOnRouteMbo = ComissionMembersSet.addAtEnd();
                        ComissionOnRouteMbo.setValue("REAILABORCODE", laborMbo.getString("REAILABORCODE"), 11L);
                    }
                }
            }
        }
    }
}
