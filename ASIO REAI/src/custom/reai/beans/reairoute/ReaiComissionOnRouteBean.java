package custom.reai.beans.reairoute;

import psdi.mbo.MboRemote;
import psdi.mbo.MboSetRemote;
import psdi.mbo.SqlFormat;
import psdi.util.MXException;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.beans.MultiselectDataBean;
import psdi.webclient.system.controller.ControlInstance;
import psdi.webclient.system.controller.Utility;
import psdi.webclient.system.controller.WebClientEvent;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Vector;

public class ReaiComissionOnRouteBean
        extends MultiselectDataBean
{
    protected MboSetRemote getMboSetRemote()
            throws MXException, RemoteException
    {
        DataBean appBean = this.app.getAppBean();
        MboRemote inspMbo = appBean.getMboSet().getMbo(0);
        MboSetRemote laborSet = getLaborSet(inspMbo);
        return laborSet;
    }

    public MboSetRemote getLaborSet(MboRemote inspMbo)
            throws MXException, RemoteException
    {
        ControlInstance InspSet = this.clientSession.findControl("reairoutenumcom");
        int currrow = InspSet.getDataBean().getCurrentRow();
        String RouteString = InspSet.getDataBean().getMbo(currrow).getString("REAIROUTENUM");
        SqlFormat sqlf = new SqlFormat(inspMbo.getUserInfo(), "REAIINSPNUM = :1 and orgid = :2 and siteid = :3 and reailaborcode not in (select distinct reailaborcode from maximo.REAICOMISSIONMEMBERS where reairoutenum = " + RouteString + ")");
        sqlf.setObject(1, "REAICOMISSION", "REAIINSPNUM", inspMbo.getString("REAIINSPNUM"));
        sqlf.setObject(2, "REAICOMISSION", "ORGID", inspMbo.getString("ORGID"));
        sqlf.setObject(3, "REAICOMISSION", "SITEID", inspMbo.getString("SITEID"));
        return inspMbo.getMboSet("$SELECTREAICOMISSION", "REAICOMISSION", sqlf.format());
    }

    public void copyComissionToRoute()
            throws MXException, RemoteException
    {
        WebClientEvent currentevent = this.sessionContext.getCurrentEvent();
        try
        {
            ControlInstance RouteControl = this.clientSession.findControl("reairoutenumcom");
            int currrow = RouteControl.getDataBean().getCurrentRow();
            MboRemote Route = RouteControl.getDataBean().getMbo(currrow);
            copyComissionMembers(this.getMboSetRemote(), Route);
        }
        catch (IOException e)
        {
            Utility.showMessageBox(currentevent, (RemoteException)e);
        }
    }

    public void copyComissionMembers(MboSetRemote CommissionSet, MboRemote Route)
            throws MXException, RemoteException {
        MboSetRemote ComissionMembersSet = Route.getMboSet("REAICOMISSIONMEMBERS");
        Vector ComissionVector = CommissionSet.getSelection();
        MboRemote laborMbo;
        if (ComissionVector.size() != 0) {
            int size = ComissionVector.size();
            for (int i = 0; i < size; i++) {
                if ((laborMbo = (MboRemote) ComissionVector.elementAt(i)) != null) {
                    int k = 0;
                    int sizecom = ComissionMembersSet.count();
                    for (int j = 0; j < sizecom; j++) {
                        if (ComissionMembersSet.getMbo(j).getString("REAILABORCODE").equals(laborMbo.getString("REAILABORCODE"))) {
                            k = 1;
                            break;
                        }
                    }
                    if (k == 0) {
                        MboRemote ComissionOnRouteMbo = ComissionMembersSet.addAtEnd();
                        ComissionOnRouteMbo.setValue("REAIROUTENUM", Route.getString("REAIROUTENUM"), 11L);
                        ComissionOnRouteMbo.setValue("REAILABORCODE", laborMbo.getString("REAILABORCODE"), 11L);
                    }
                }
            }
        }
        ComissionMembersSet.save();
        super.save();
        super.fireStructureChangedEvent();
        super.refreshTable();
        super.sessionContext.queueRefreshEvent();
        WebClientEvent newEvent = new WebClientEvent("dialogok", this.app.getCurrentPageId(), null, this.clientSession);
        this.clientSession.queueEvent(newEvent);
    }

    public void dialogClose()
            throws MXException, RemoteException
    {
        WebClientEvent newEvent = new WebClientEvent("dialogclose", this.app.getCurrentPageId(), null, this.clientSession);
        this.clientSession.queueEvent(newEvent);
    }
}
