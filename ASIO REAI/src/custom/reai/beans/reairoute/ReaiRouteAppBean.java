package custom.reai.beans.reairoute;

import psdi.mbo.MboRemote;
import psdi.mbo.MboSetRemote;
import psdi.util.MXApplicationException;
import psdi.util.MXException;
import psdi.webclient.system.beans.AppBean;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.WebClientEvent;
import psdi.webclient.system.runtime.WebClientRuntime;

import java.rmi.RemoteException;

public class ReaiRouteAppBean extends AppBean {

    public int REAIINPRG()
        throws MXException, RemoteException
    {
        WebClientEvent event = clientSession.getCurrentEvent();
        int msgRet = event.getMessageReturn();
        if (msgRet < 0) {
            throw new MXApplicationException("REAI", "routeinprog");
        }
        else if (msgRet == WebClientRuntime.MSG_BTNYES) {
            DataBean appBean = this.app.getAppBean();
            MboRemote routeMbo = appBean.getMbo();
            routeMbo.setValue("REAISTATUS", "ГОТОВ К ЗАВЕРШЕНИЮ", 2L);
            this.save();
            this.fireStructureChangedEvent();
            this.refreshTable();
            this.sessionContext.queueRefreshEvent();
            return 1;
        }
        return 1;
    }

    public int REAIROUTEFACT()
        throws MXException, RemoteException
    {
        WebClientEvent event = clientSession.getCurrentEvent();
        int msgRet = event.getMessageReturn();
        if (msgRet < 0) {
            throw new MXApplicationException("REAI", "routesoglfact");
        }
        else if (msgRet == WebClientRuntime.MSG_BTNYES) {
            DataBean appBean = this.app.getAppBean();
            MboRemote routeMbo = appBean.getMbo();
            MboSetRemote factMbo = routeMbo.getMboSet("REAIFACTCHERN");
            int countFact = factMbo.count();
            for (int i = 0; i < countFact; i++) {
                factMbo.getMbo(i).setValue("REAISTATUS", "СОГЛАСОВАНО", 11L);
            }
            this.save();
            this.fireStructureChangedEvent();
            this.refreshTable();
            this.sessionContext.queueRefreshEvent();
            return 1;
        }
        return 1;
    }
}
