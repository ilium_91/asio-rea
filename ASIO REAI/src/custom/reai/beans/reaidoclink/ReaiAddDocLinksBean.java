package custom.reai.beans.reaidoclink;

import psdi.mbo.MboSetRemote;
import psdi.util.MXException;
import psdi.webclient.system.beans.AppBean;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.ControlHandler;
import psdi.webclient.system.controller.Utility;
import psdi.webclient.system.controller.WebClientEvent;

import java.rmi.RemoteException;

public class ReaiAddDocLinksBean
        extends ReaiRegisterDocBean
{
    DataBean attachments = null;

    protected MboSetRemote getMboSetRemote()
            throws MXException, RemoteException
    {
        ControlHandler handler = this.creatingEvent.getSourceControl();
        String dataSrc = handler.getProperty("datasrc");
        this.attachments = this.app.getDataBean(dataSrc);
        this.parent = this.attachments.getParent();
        return this.attachments.getMboSet();
    }

    public synchronized void insert()
            throws MXException, RemoteException
    {
        super.insert();
        setValue("ADDINFO", "TRUE", 11L);

        setValue("document", this.attachments.getMboSet().getMbo().getMboSet("DOCINFO").getMbo().getString("document"), 11L);
        dataChangedEvent(this);
    }


    public synchronized void close()
    {
        if (this.dialogReferences > 0)
        {
            this.dialogReferences -= 1;
            return;
        }
        resetDataBean();
        cleanup();
    }

    public int execute()
            throws MXException, RemoteException
    {
        int retVal = super.execute();
        validate();
        try
        {
            if (this.app.getAppBean() != null) {
                ((AppBean)this.app.getAppBean()).saveattachment();
            } else {
                save();
            }
        }
        catch (MXException e)
        {
            WebClientEvent currentEvent = this.sessionContext.getCurrentEvent();
            Utility.showMessageBox(currentEvent, e);
            getMboSet().reset();
            return 1;
        }
        if (this.parent != null) {
            this.parent.fireStructureChangedEvent();
        }
        return retVal;
    }

    public int cancelDialog()
            throws MXException, RemoteException
    {
        DataBean parentBean = getParent();
        if (parentBean != null) {
            parentBean.getMboSet().reset();
        }
        getMboSet().reset();
        return 1;
    }

    public int passSuperExecute()
            throws MXException, RemoteException
    {
        int retVal = super.execute();
        return retVal;
    }

    public int passSuperCancelDialog()
            throws MXException, RemoteException
    {
        int retVal = super.cancelDialog();
        return retVal;
    }
}
