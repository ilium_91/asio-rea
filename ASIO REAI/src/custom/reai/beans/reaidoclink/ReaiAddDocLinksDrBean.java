package custom.reai.beans.reaidoclink;

import psdi.mbo.MboRemote;
import psdi.util.MXException;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.Utility;
import psdi.webclient.system.controller.WebClientEvent;

import java.rmi.RemoteException;

public class ReaiAddDocLinksDrBean
        extends ReaiAddDocLinksBean
{
    public int execute()
            throws MXException, RemoteException
    {
        int retVal = super.passSuperExecute();
        validate();
        try
        {
            save();
        }
        catch (MXException e)
        {
            WebClientEvent currentEvent = this.sessionContext.getCurrentEvent();
            Utility.showMessageBox(currentEvent, e);
            getMboSetRemote().reset();

            return 1;
        }
        if (this.parent != null) {
            this.parent.fireStructureChangedEvent();
        }
        return retVal;
    }

    public void save()
            throws MXException
    {
        if (this.mboSetRemote == null) {
            return;
        }
        try
        {
            boolean fromMainApp = true;
            DataBean appbean = this.app.getAppBean();
            if ((this.parent != null) && (this.parent != appbean)) {
                fromMainApp = false;
            }
            boolean parentBoundToTable = false;
            if ((this.parent != null) && (this.parent.boundToTable())) {
                parentBoundToTable = true;
            }
            if ((fromMainApp) || (!parentBoundToTable)) {
                this.mboSetRemote = this.app.getAppBean().getMboSet();
            }
            if (this.mboSetRemote == null) {
                return;
            }
            MboRemote newMbo = this.mboSetRemote.getMbo();
            if (newMbo != null)
            {
                int row = this.parent.getCurrentRow();
                if (newMbo.toBeAdded()) {
                    try
                    {
                        long uniqueId = this.app.getAppBean().getUniqueIdValue();
                        this.mboSetRemote.save();
                        newMbo = this.app.getAppBean().getMboForUniqueId(uniqueId);
                    }
                    catch (NullPointerException e) {}
                } else {
                    appbean.save();
                }
                this.clientSession.addMXWarnings(this.mboSetRemote.getWarnings());
                getParent().setCurrentRow(row);
                if (fromMainApp) {
                    newMbo = this.mboSetRemote.moveTo(this.currentRow);
                }
                if (newMbo == null)
                {
                    this.currentRecordData = null;
                    this.currentRow = -1;
                }
                else
                {
                    this.currentRecordData = newMbo.getMboData(this.app.getAppBean().getAttributes());
                }
                fireStructureChangedEvent();
            }
        }
        catch (RemoteException e)
        {
            handleRemoteException(e);
        }
    }

    public int cancelDialog()
            throws MXException, RemoteException
    {
        super.passSuperCancelDialog();
        return 1;
    }
}
