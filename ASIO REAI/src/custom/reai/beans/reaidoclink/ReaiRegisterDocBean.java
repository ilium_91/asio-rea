//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package custom.reai.beans.reaidoclink;

import psdi.app.doclink.DoclinkServiceRemote;
import psdi.mbo.MboRemote;
import psdi.mbo.MboSetRemote;
import psdi.mbo.SqlFormat;
import psdi.mbo.Translate;
import psdi.server.MXServer;
import psdi.util.MXApplicationException;
import psdi.util.MXException;
import psdi.util.MXFormat;
import psdi.util.MXSession;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.UploadFile;
import psdi.webclient.system.runtime.WebClientRuntime;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.Properties;
import java.util.Vector;

public class ReaiRegisterDocBean extends DataBean {
    String ownername = "";

    public ReaiRegisterDocBean() {
    }

    public void initialize() throws MXException, RemoteException {
        try {
            this.getMboSet().checkMethodAccess("add");
        } catch (MXException var2) {
            this.clientSession.showMessageBox(this.clientSession.getCurrentEvent(), var2);
            this.app.popPage(this.app.getCurrentPage(), true);
        }

        super.initialize();
        MboRemote owner = this.getMboSetRemote().getOwner();
        if (owner != null) {
            this.ownername = owner.getName();
            owner.getMboSet("REAIDOCLINKS");
        }

        this.insert();
    }

    public synchronized void insert() throws MXException, RemoteException {
        MboSetRemote list = null;
        MboRemote mbo = null;
        super.insert();
        this.setValue("APP", this.app.getId().toUpperCase(), 11L);
        if ((list = this.getList("doctype")) != null && (mbo = list.getMbo(0)) != null) {
            this.setValue("doctype", mbo.getString("doctype"), 11L);
        }

    }

    public int execute() throws MXException, RemoteException {
        try {
            SqlFormat sqlf = new SqlFormat(this.getMbo().getUserInfo(), "app = :1");
            sqlf.setObject(1, "MAXAPPS", "APP", this.app.getId());
            MboSetRemote mboSet = this.getMbo().getMboSet("$MAXAPP", "MAXAPPS", sqlf.format());
            String originalAppName = mboSet.getMbo(0).getString("originalapp");
            Translate tr = MXServer.getMXServer().getMaximoDD().getTranslator();
            String urltype = tr.toInternalString("URLTYPE", this.getString("urltype"));
            if (urltype.equals("FILE")) {
                String absFileName = "";
                String fullFileName = "";
                String fileName = "";
                String invalidCharacters = null;
                String[] invalidChars = null;
                String invalidLeadingCharacters = null;
                String[] invalidLeadingChars = null;
                InputStream invalidfilename = null;
                UploadFile df = (UploadFile)this.app.get("doclinkfile");
                String filePromptText = WebClientRuntime.getWebClientSystemProperty("mxe.doclink.usefileprompt", "0");
                boolean useFilePrompt = filePromptText.equals("1");
                String allowedExtentions;
                int i;
                if (!this.getBoolean("upload") && !useFilePrompt) {
                    fullFileName = this.getString("urlname");
                    fullFileName = fullFileName.substring(0, 1).toUpperCase() + fullFileName.substring(1);
                    fileName = fullFileName.substring(fullFileName.lastIndexOf(File.separator) + 1);
                    absFileName = "." + fullFileName.substring(fullFileName.lastIndexOf(File.separator));
                    i = fileName.lastIndexOf("\\");
                    int x2 = fileName.lastIndexOf("/");
                    if (x2 > i) {
                        i = x2;
                    }

                    fileName = fileName.substring(i + 1).toLowerCase();
                } else if (df == null) {
                    if (this.getString("urlname").length() == 0) {
                        return 1;
                    }

                    fullFileName = this.getString("urlname");
                    fullFileName = fullFileName.substring(0, 1).toUpperCase() + fullFileName.substring(1);
                    fileName = fullFileName.substring(fullFileName.lastIndexOf(File.separator) + 1);
                    absFileName = "." + fullFileName.substring(fullFileName.lastIndexOf(File.separator));
                } else {
                    absFileName = df.getAbsoluteFileName();
                    fullFileName = df.getFullFileName();
                    fileName = df.getFileName();
                    if (this.getBoolean("upload")) {
                        MXSession mxs = this.clientSession.getMXSession();
                        DoclinkServiceRemote doclinkService = (DoclinkServiceRemote)mxs.getMXServerRemote().lookup("DOCLINK");
                        allowedExtentions = doclinkService.getDefaultFilePath(this.getString("doctype"), mxs.getUserInfo());
                        if (WebClientRuntime.isNull(allowedExtentions)) {
                            allowedExtentions = MXServer.getMXServer().getProperty("mxe.doclink.doctypes.defpath");
                        }

                        if (WebClientRuntime.isNull(allowedExtentions)) {
                            allowedExtentions = File.separator + "DOCLINKS" + File.separator + "default";
                        }

                        try {
                            df.setDirectoryName(allowedExtentions);
                            df.writeToDisk();
                            absFileName = df.getAbsoluteFileName();
                            if (!WebClientRuntime.isNull(absFileName)) {
                                df.save();
                            }
                        } catch (IOException var35) {
                            var35.printStackTrace();
                        }
                    }
                }

                if (fileName.indexOf("\u0000") != -1) {
                    Object[] params = new Object[]{"\u0000"};
                    throw new MXApplicationException("doclink", "errorattachdocinvalidchars", params);
                }

                try {
                    invalidfilename = this.getClass().getResourceAsStream("/invalidAttachmentFilename.properties");
                    Properties invalidcharProp = new Properties();
                    invalidcharProp.load(invalidfilename);
                    invalidCharacters = invalidcharProp.getProperty("mxe.doclink.filename.invalidCharacters");
                    invalidLeadingCharacters = invalidcharProp.getProperty("mxe.doclink.filename.invalidLeadingCharacters");
                } catch (Exception var34) {
                    System.out.println("++++ File invalidAttachmentFilename.properties doesn't exist ");
                    var34.printStackTrace();
                } finally {
                    if (invalidfilename != null) {
                        try {
                            invalidfilename.close();
                        } catch (Exception var33) {
                            var33.printStackTrace();
                        }
                    }

                }

                Object[] params;
                if (invalidCharacters != null) {
                    invalidChars = invalidCharacters.split(",");

                    for(i = 0; i < invalidChars.length; ++i) {
                        if (fileName.indexOf(invalidChars[i].trim()) != -1) {
                            params = new Object[]{invalidChars[i]};
                            throw new MXApplicationException("doclink", "errorattachdocinvalidchars", params);
                        }
                    }
                }

                if (invalidLeadingCharacters != null) {
                    invalidLeadingChars = invalidLeadingCharacters.split(",");

                    for(i = 0; i < invalidLeadingChars.length; ++i) {
                        if (fileName.startsWith(invalidLeadingChars[i].trim())) {
                            params = new Object[]{invalidLeadingChars[i]};
                            throw new MXApplicationException("doclink", "errordoclinkinvalidleadingchars", params);
                        }
                    }
                }

                String fileExtension = "";
                Vector<String> printableList = new Vector();
                printableList.add("pdf");
                printableList.add("csv");
                printableList.add("txt");
                printableList.add("doc");
                printableList.add("gif");
                printableList.add("jpg");
                printableList.add("xls");
                printableList.add("ppt");
                printableList.add("pptx");
                printableList.add("docx");
                printableList.add("xlsx");
                printableList.add("png");
                allowedExtentions = MXServer.getMXServer().getProperty("mxe.doclink.doctypes.allowedFileExtensions");
                String[] allowedExts = allowedExtentions.split(",");
                boolean isAllowedtype = false;
                if (fileName.lastIndexOf(".") != -1) {
                    fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
                }

                for(i = 0; i < allowedExts.length; ++i) {
                    if (allowedExts[i].equalsIgnoreCase(fileExtension)) {
                        isAllowedtype = true;
                        break;
                    }
                }

                if (fileExtension.equals("")) {
                    isAllowedtype = true;
                }

                if (!isAllowedtype && !fileName.equals("unknown")) {
                    df.deleteFileOnDisk();
                    Object[] param = new Object[]{fileExtension, allowedExtentions};
                    throw new MXApplicationException("doclink", "errordoclinkinvalidextension", param);
                }

                boolean isPrintthrulink = true;
                this.getString("printthrulinkdflt");
                this.getString("printthrulink");
                MXSession mxs = this.clientSession.getMXSession();
                String localeYes = MXFormat.getDisplayYesValue(mxs.getLocale());
                isPrintthrulink = this.getString("printthrulink").equals(localeYes) || this.getString("printthrulinkdflt").equals(localeYes);
                String appname;
                if (isPrintthrulink && !fileName.equals("unknown")) {
                    isAllowedtype = true;
                    if (!printableList.contains(fileExtension)) {
                        appname = this.app.getId();
                        if (!appname.equalsIgnoreCase("createsr") && !appname.equalsIgnoreCase("viewsr") && !this.ownername.equalsIgnoreCase("commlog")) {
                            isAllowedtype = false;
                        }
                    }
                }

                if (!isAllowedtype) {
                    appname = this.app.getId();
                    if (!appname.equalsIgnoreCase("createsr") && !appname.equalsIgnoreCase("viewsr") && !this.ownername.equalsIgnoreCase("commlog")) {
                        throw new MXApplicationException("doclink", "errorprintthrulink");
                    }
                }

                this.setValue("urlname", fullFileName, 11L);
                this.setValue("newurlname", absFileName, 11L);
                this.app.remove("doclinkfile");
            } else if (!urltype.equals("WWW") && urltype.equals("DMS")) {
                ;
            }

            if (originalAppName.equalsIgnoreCase("CREATESR") || this.app.getId().equalsIgnoreCase("CREATESR")) {
                DataBean tableBean = this.app.getDataBean("attachments");
                tableBean.refreshTable();
            }

            return 1;
        } catch (MXException var37) {
            throw var37;
        }
    }

    public int cancelDialog() throws MXException, RemoteException {
        if (!this.app.getId().equalsIgnoreCase("CREATESR")) {
            this.getMboSet().reset();

            try {
                this.validate();
            } catch (MXException var2) {
                this.mboSetRemote.deleteAndRemove(this.getMbo(), 2L);
            }
        } else {
            this.mboSetRemote.deleteAndRemove(this.getMbo(), 2L);
        }

        return 1;
    }
}
