package custom.reai.beans.reaiclassification;

import psdi.app.assetcatalog.AssetCatalogServiceRemote;
import psdi.app.assetcatalog.ClassStructureRemote;
import psdi.app.assetcatalog.ClassStructureSetRemote;
import psdi.mbo.MboRemote;
import psdi.mbo.MboSetRemote;
import psdi.util.MXException;
import psdi.util.MXSession;
import psdi.webclient.beans.common.TreeControlBean;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.*;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Hashtable;

public class ReaiTreeBean extends TreeControlBean {
    private DataBean originalBean = null;

    public void initialize()
            throws MXException, RemoteException
    {
        try
        {
            super.initialize();
            ControlInstance originalControl = this.creatingEvent.getSourceControlInstance();
            this.originalBean = this.clientSession.getDataBean(originalControl.getProperty("datasrc"));

            ClassStructureSetRemote classStructSet = (ClassStructureSetRemote)getMboSet();
            classStructSet.setOriginatingObject(this.originalBean.getMbo());
            if (classStructSet.getApp() == null)
            {
                String appName = this.app.getId();
                if (appName != null)
                {
                    classStructSet.setIsLookup(true);
                    classStructSet.setApp(appName.toUpperCase());
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public int selectrecord()
            throws MXException
    {
        WebClientEvent event = this.sessionContext.getCurrentEvent();
        try
        {
            super.selectrecord();
            updateOriginatingRecord();
        }
        catch (MXException e)
        {
            Utility.sendEvent(new WebClientEvent("dialogclose", this.app.getCurrentPageId(), null, this.sessionContext));
            Utility.showMessageBox(event, e);
        }
        catch (RemoteException m)
        {
            Utility.sendEvent(new WebClientEvent("dialogclose", this.app.getCurrentPageId(), null, this.sessionContext));
            Utility.showMessageBox(event, m);
        }
        return 1;
    }

    protected void updateOriginatingRecord()
            throws MXException, RemoteException
    {
        String uniqueIdSelected = this.sessionContext.getCurrentEvent().getValueString();

        String originalAttr = null;
        MboSetRemote originalSet = null;

        Object eventVal = this.creatingEvent.getValue();
        Hashtable popupInfo = null;
        boolean expBuilder = false;
        if ((eventVal != null) && ((eventVal instanceof Hashtable)))
        {
            popupInfo = (Hashtable)eventVal;
            if (popupInfo.containsKey("c_datasrc")) {
                originalSet = this.clientSession.getDataBean(popupInfo.get("c_datasrc").toString()).getMboSet();
            }
            if (popupInfo.containsKey("c_attribute")) {
                originalAttr = popupInfo.get("c_attribute").toString();
            }
            expBuilder = true;
        }
        else
        {
            ComponentInstance compInst = this.creatingEvent.getSourceComponentInstance();
            originalAttr = compInst.getProperty("dataattribute");
            if (((compInst instanceof BoundComponentInstance)) && (originalAttr != null) && (originalAttr.equalsIgnoreCase("CLASSSTRUCTURE.HIERARCHYPATH"))) {
                ((BoundComponentInstance)compInst).readOnlyCheck();
            }
            originalSet = this.originalBean.getMboSet();
        }
        MboRemote selectedClassMbo = getMbo();
        if (selectedClassMbo == null)
        {
            MXSession mxs = getMXSession();
            AssetCatalogServiceRemote assetCatService = (AssetCatalogServiceRemote)mxs.lookup("ASSETCATALOG");
            selectedClassMbo = assetCatService.getClassStructure(mxs.getUserInfo(), uniqueIdSelected);
        }
        if (selectedClassMbo.getThisMboSet().count() == 1)
        {
            String objectName = originalSet.getName().toUpperCase();
            MboRemote useWith = ((ClassStructureRemote)selectedClassMbo).getUseWith(objectName);
            if (useWith == null)
            {
                Utility.sendEvent(new WebClientEvent("dialogclose", this.app.getCurrentPageId(), null, this.sessionContext));
                return;
            }
        }
        MboRemote originalRecord = originalSet.getMbo();
        if (originalRecord == null) {
            originalRecord = originalSet.getMbo(0);
        }
        if ((originalRecord != null) && (selectedClassMbo != null))
        {
            String classAttr = null;
            String hierarchyAttr = null;
            if (originalAttr.equalsIgnoreCase("CLASSSTRUCTURE.HIERARCHYPATH"))
            {
                classAttr = "classstructureid";
                hierarchyAttr = "CLASSSTRUCTURE.HIERARCHYPATH";
            }
            else if (!expBuilder)
            {
                int index = originalAttr.indexOf(".");
                classAttr = originalAttr.substring(0, index);
                if (classAttr.equalsIgnoreCase("CLASSSTRUCTURE")) {
                    classAttr = "classstructureid";
                }
                hierarchyAttr = originalAttr;
            }
            else
            {
                hierarchyAttr = originalAttr;
            }

            if(!(this.originalBean.getMbo().getString("classstructureid")).equals(selectedClassMbo.getString("classstructureid")))

            {
                this.originalBean.setValue("REAITYPICAL", "");
                this.originalBean.setValue(classAttr, selectedClassMbo.getString("classstructureid"), 11L) ;

            }



            String hierarchypath = originalRecord.getString(hierarchyAttr);
            if (expBuilder) {
                this.originalBean.setValue(hierarchyAttr, hierarchypath + selectedClassMbo.getString("hierarchypath"), 11L);
            } else {
                this.originalBean.setValue(hierarchyAttr, hierarchypath, 11L);
            }
            Utility.sendEvent(new WebClientEvent("dialogclose", this.app.getCurrentPageId(), null, this.sessionContext));
        }
    }

    private boolean isfrominitialize = false;

    protected synchronized boolean moveTo(int row)
            throws MXException, RemoteException
    {
        if ((row == 0) && (this.isfrominitialize))
        {
            this.isfrominitialize = false;
            return true;
        }
        return super.moveTo(row);
    }
}
