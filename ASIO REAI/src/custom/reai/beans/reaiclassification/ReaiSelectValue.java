package custom.reai.beans.reaiclassification;

import psdi.mbo.MboSetRemote;
import psdi.util.MXException;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.ControlInstance;
import psdi.webclient.system.controller.Utility;
import psdi.webclient.system.controller.WebClientEvent;
import psdi.webclient.system.runtime.WebClientRuntime;

import java.io.IOException;
import java.rmi.RemoteException;

public class ReaiSelectValue extends DataBean {

    protected MboSetRemote getMboSetRemote()
            throws MXException, RemoteException
    {
        try
        {
            MboSetRemote mboSetRemote = super.getMboSetRemote();
            return mboSetRemote;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public int selectrecord()
            throws MXException
    {
        WebClientEvent currentevent = this.sessionContext.getCurrentEvent();
        try
        {
            super.selectrecord();
            ControlInstance originalControl = this.creatingEvent.getSourceControlInstance();
            DataBean originalsBean = this.clientSession.getDataBean(originalControl.getProperty("datasrc"));
            String CL = this.parent.getMbo().getString("classstructureid");
            String TYPICAL = getMbo().getString("ASSETATTRID");
            originalsBean.setValue("CLASSSTRUCTUREID", CL, 2L);
            originalsBean.setValue("REAITYPICAL", TYPICAL, 2L);
            WebClientRuntime.sendEvent(new WebClientEvent("dialogclose", this.app.getCurrentPageId(), null, this.clientSession));
        }
        catch (IOException e)
        {
            Utility.showMessageBox(currentevent, (RemoteException)e);
        }
        return 1;
    }
}
