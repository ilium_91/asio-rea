package custom.reai.beans.reaifact;

import custom.reai.app.reaifact.ReaiFactRemote;
import psdi.mbo.MboRemote;
import psdi.util.MXApplicationException;
import psdi.util.MXException;
import psdi.webclient.system.beans.AppBean;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.Utility;
import psdi.webclient.system.controller.WebClientEvent;
import psdi.webclient.system.runtime.WebClientRuntime;

import java.rmi.RemoteException;

public class ReaiFactAppBean extends AppBean {

    public int REAISOGL()
            throws MXException, RemoteException
    {
        DataBean appBean = this.app.getAppBean();
        MboRemote factMbo = appBean.getMbo();
        factMbo.setValue("REAISTATUS", "СОГЛАСОВАНО", 11L); // проставили статус согласовано
        this.save();
        this.fireStructureChangedEvent();
        this.refreshTable();
        this.sessionContext.queueRefreshEvent();
        return 1;
    }

    public int REAIDELETE()
            throws MXException, RemoteException
    {
        WebClientEvent event = clientSession.getCurrentEvent();
        int msgRet = event.getMessageReturn();
        if (msgRet < 0)
        {
            throw new MXApplicationException("REAI", "delete");
        }
        else if (msgRet == WebClientRuntime.MSG_BTNYES)
        {
            DataBean appBean = this.app.getAppBean();
            MboRemote factMbo = appBean.getMbo();
            factMbo.setValue("REAISTATUS", "УДАЛЕНО", 11L); // проставили статус к удалению
            this.save();
            this.fireStructureChangedEvent();
            this.refreshTable();
            this.sessionContext.queueRefreshEvent();
            return 1;
        }
        return 1;
    }

    public int DUPLICATEWP()
            throws MXException, RemoteException
    {
        try
        {


            WebClientEvent event = clientSession.getCurrentEvent();
            int msgRet = event.getMessageReturn();
            if (msgRet < 0)
            {
                throw new MXApplicationException("REAI", "duplicateyesno");
            }
            else if (msgRet == WebClientRuntime.MSG_BTNYES)
            {
                this.duplicateWithPhotoMbo();
                Utility.showMessageBox(this.sessionContext.getCurrentEvent(), "reai", "duprecord", null);
                this.sessionContext.queueRefreshEvent();
            }
        }
        catch (MXException mxe)
        {
            Utility.showMessageBox(this.sessionContext.getCurrentEvent(), mxe);
        }
        return 1;
    }

    public synchronized boolean duplicateWithPhotoMbo() throws RemoteException, MXException {
        ReaiFactRemote mboRemote = (ReaiFactRemote) this.getMbo();
        if (mboRemote != null) {
            if (mboRemote.toBeValidated()) {
                mboRemote.validate();
            }
            ReaiFactRemote newMbo = mboRemote.duplicateWithPhoto();
            if (newMbo != null) {
                this.currentRow = this.mboSetRemote.getCurrentPosition();
                this.invalidateTableData();
                this.setCurrentRecordData(newMbo);
                if (this.saveCount > -1) {
                    ++this.saveCount;
                }
                return true;
            }
        }
        return false;
    }

    public int DUPLICATENOP()
            throws MXException, RemoteException
    {
        try
        {


            WebClientEvent event = clientSession.getCurrentEvent();
            int msgRet = event.getMessageReturn();
            if (msgRet < 0)
            {
                throw new MXApplicationException("REAI", "duplicateyesnop");
            }
            else if (msgRet == WebClientRuntime.MSG_BTNYES)
            {
                this.duplicateMboNoPhoto();
                Utility.showMessageBox(this.sessionContext.getCurrentEvent(), "reai", "duprecord", null);
                this.sessionContext.queueRefreshEvent();
            }
        }
        catch (MXException mxe)
        {
            Utility.showMessageBox(this.sessionContext.getCurrentEvent(), mxe);
        }
        return 1;
    }

    public synchronized boolean duplicateMboNoPhoto() throws RemoteException, MXException {
        ReaiFactRemote mboRemote = (ReaiFactRemote) this.getMbo();
        if (mboRemote != null) {
            if (mboRemote.toBeValidated()) {
                mboRemote.validate();
            }
            ReaiFactRemote newMbo = mboRemote.duplicateNoPhoto();
            if (newMbo != null) {
                this.currentRow = this.mboSetRemote.getCurrentPosition();
                this.invalidateTableData();
                this.setCurrentRecordData(newMbo);
                if (this.saveCount > -1) {
                    ++this.saveCount;
                }
                return true;
            }
        }
        return false;
    }


}