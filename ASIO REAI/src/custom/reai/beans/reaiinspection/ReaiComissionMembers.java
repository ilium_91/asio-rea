package custom.reai.beans.reaiinspection;

import psdi.mbo.MboRemote;
import psdi.mbo.MboSetRemote;
import psdi.mbo.SqlFormat;
import psdi.util.MXException;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.beans.MultiselectDataBean;
import psdi.webclient.system.controller.Utility;
import psdi.webclient.system.controller.WebClientEvent;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Vector;

public class ReaiComissionMembers
        extends MultiselectDataBean
{
    protected MboSetRemote getMboSetRemote() //вывели записи из LABOR в интерфейс приложухи
            throws MXException, RemoteException
    {
        DataBean appBean = this.app.getAppBean();
        MboRemote inspMbo = appBean.getMbo();
        MboSetRemote expertSet = getExpertSet(inspMbo);
        return expertSet;
    }

    public MboSetRemote getExpertSet(MboRemote inspMbo) // получили записи из Labor
            throws MXException, RemoteException
    {
        SqlFormat sqlf = new SqlFormat(inspMbo.getUserInfo(), "personid in (select personid from maximo.person where persongroup in ('OS1472', 'OS1914', '1472', '1914')  and personid in (select distinct personid from maximo.maxuser where defsite = :1))");
        sqlf.setObject(1, "MAXUSER", "DEFSITE", inspMbo.getString("SITEID"));

        MboSetRemote mboSet = inspMbo.getMboSet("$SELECTEXPERT", "LABOR", sqlf.format());
        return mboSet;
    }

    public void copyExpertToInspection()
            throws MXException
    {
        WebClientEvent currentevent = this.sessionContext.getCurrentEvent();
        try
        {
            String inspNum = this.app.getDataBean().getMbo().getString("REAIINSPNUM");
            copyComission(inspNum, getMboSetRemote());
        }
        catch (IOException e)
        {
            Utility.showMessageBox(currentevent, (RemoteException)e);
        }
    }

    public void copyComission(String insp, MboSetRemote CommissionSet)
            throws MXException, RemoteException
    {
        WebClientEvent currentevent = this.sessionContext.getCurrentEvent();
        try
        {
            Vector ExpertVector = CommissionSet.getSelection();// записи чекнутых экспертов
            if (ExpertVector.size() == 0)
            {
                WebClientEvent newEvent = new WebClientEvent("dialogclose", this.app.getCurrentPageId(), null, this.clientSession);
                this.clientSession.queueEvent(newEvent);
            }
            // записи комисиий
            int size = ExpertVector.size();// размер выбранных экспертов
            MboRemote ExpertMbo;// запись эксперта
            MboSetRemote mboSet = this.app.getDataBean().getMbo().getMboSet("REAICOMISSION");
            int sizecom = mboSet.count();
            for (int i = 0; i < size; i++)
            {
                if ((ExpertMbo = (MboRemote)ExpertVector.elementAt(i)) != null)// запись эксперта равна выбранному эксперту
                {
                    int k = 0;
                    for (int j = 0; j < sizecom; j++)
                    {
                        if (mboSet.getMbo(j).getString("REAILABORCODE").equals(ExpertMbo.getString("LABORCODE"))) {
                            k = 1;
                            break;
                        }
                    }
                    if (k == 0)
                    {
                        MboRemote comissionMbo = mboSet.addAtEnd();
                        comissionMbo.setValue("REAILABORCODE", ExpertMbo.getString("laborcode"), 11L);
                        comissionMbo.setValue("REAIINSPNUM", insp, 11L);
                    }
                }
            }
            mboSet.save();
            super.save();
            super.fireStructureChangedEvent();
            super.refreshTable();
            super.sessionContext.queueRefreshEvent();
            WebClientEvent newEvent = new WebClientEvent("dialogok", this.app.getCurrentPageId(), null, this.clientSession);
            this.clientSession.queueEvent(newEvent);
        }
        catch (IOException e)
        {
            Utility.showMessageBox(currentevent, (RemoteException)e);
        }
    }

    public void dialogClose()
            throws MXException, RemoteException
    {
        WebClientEvent newEvent = new WebClientEvent("dialogclose", this.app.getCurrentPageId(), null, this.clientSession);
        this.clientSession.queueEvent(newEvent);
    }
}

