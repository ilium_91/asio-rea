package custom.reai.beans.reaiinspection;


import psdi.mbo.MboRemote;
import psdi.mbo.MboSetRemote;
import psdi.util.MXException;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.WebClientEvent;

import java.rmi.RemoteException;



public class ReaiInspectionFactSogl extends DataBean {

    public void REAIROUTEFACT() throws MXException, RemoteException {
        WebClientEvent event = this.sessionContext.getCurrentEvent();
        MboRemote routeMbo = this.getMbo(event.getRow());
        MboSetRemote factSetMbo = routeMbo.getMboSet("REAIFACTCHERN");
        int countfact = factSetMbo.count();
/*        if (countfact == 0) {
            return;
        }
        else {*/
            for (int j = 0; j < countfact; j++) {
                MboRemote factMbo = factSetMbo.getMbo(j);
                factMbo.setValue("REAISTATUS", "СОГЛАСОВАНО", 11L);
            }

            this.save();
            this.fireStructureChangedEvent();
            this.refreshTable();
            this.sessionContext.queueRefreshEvent();
        }
    }

