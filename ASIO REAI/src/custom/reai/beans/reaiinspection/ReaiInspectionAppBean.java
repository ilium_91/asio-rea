package custom.reai.beans.reaiinspection;


import custom.reai.app.reairoute.ReaiRouteSet;
import psdi.app.doclink.AppDoctypeRemote;
import psdi.app.doclink.AppDoctypeSetRemote;
import psdi.app.doclink.DoctypesRemote;
import psdi.app.doclink.DoctypesSetRemote;
import psdi.mbo.MboRemote;
import psdi.mbo.MboSetRemote;
import psdi.server.MXServer;
import psdi.util.MXApplicationException;
import psdi.util.MXException;
import psdi.webclient.system.beans.AppBean;
import psdi.webclient.system.beans.DataBean;
import psdi.webclient.system.controller.Utility;
import psdi.webclient.system.controller.WebClientEvent;
import psdi.webclient.system.runtime.WebClientRuntime;

import java.io.File;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Objects;
import java.util.Properties;

public class ReaiInspectionAppBean extends AppBean {

    public ReaiInspectionAppBean() {
    }

    public int REAIDRAFT()
            throws MXException, RemoteException {
        DataBean appBean = this.app.getAppBean();
        MboRemote inspectionMbo = appBean.getMbo();
        boolean k = false;
        MboSetRemote routecheck = inspectionMbo.getMboSet("REAIROUTE");
        int countroutecheck = routecheck.count();
        for (int j = 0; j < countroutecheck; j++) {
            MboRemote routecheckMbo = routecheck.getMbo(j);
            MboSetRemote reaifactSet = routecheckMbo.getMboSet("REAIFACT");
            int countfact = reaifactSet.count();
            if (countfact != 0) {
            k = true;
            break;
            }
        }
        if (k) {
            Utility.showMessageBox(this.sessionContext.getCurrentEvent(), "REAI", "FactNotNull", null);
        }
        else {
            inspectionMbo.setValue("REAISTATUS", "ЧЕРНОВИК", 11L);
            ReaiRouteSet routeSet = (ReaiRouteSet) inspectionMbo.getMboSet("REAIROUTEPENDING");
            int countroute = routeSet.count();
            for (int i = 0; i < countroute; i++) {
                MboRemote routeMbo = routeSet.getMbo(i);
                routeMbo.setValue("REAISTATUS", "ЧЕРНОВИК", 11L);
            }
            this.save();
            this.fireStructureChangedEvent();
            this.refreshTable();
            this.sessionContext.queueRefreshEvent();
             }
        return 1;
    }



    public int REAIPENDING()
            throws MXException, RemoteException {
        DataBean appBean = this.app.getAppBean();
        MboRemote inspectionMbo = appBean.getMbo();
        MboSetRemote comissionCheckSet = inspectionMbo.getMboSet("REAICOMISSION");
        MboSetRemote routeCheckSet = inspectionMbo.getMboSet("REAIROUTE");
        if (comissionCheckSet.count() == 0 || routeCheckSet.count() == 0) {
            Utility.showMessageBox(this.sessionContext.getCurrentEvent(), "REAI", "faultinspdata", null);
            return 1;
        } else {
            boolean k = true;
            for (int j = 0; j < routeCheckSet.count(); j++) {
                MboSetRemote routeComissionCheckSet = routeCheckSet.getMbo(j).getMboSet("REAICOMISSIONMEMBERS");
                if (routeComissionCheckSet.count() == 0) {
                    k = false;
                    break;
                }
            }
            if (k) {
                String inspCode = inspectionMbo.getString("REAICODE");
                inspectionMbo.setValue("REAISTATUS", "ПЕРЕДАНО В РАБОТУ", 11L); // проставили статус черновик
                ReaiRouteSet routeSet = (ReaiRouteSet) inspectionMbo.getMboSet("REAIROUTEDRAFT"); // получаем взаимосвязь с маршрутами
                int countroute = routeSet.count(); // получаем число маршрутов
                Properties config = MXServer.getMXServer().getConfig();
                String propVal = config.getProperty("mxe.doclink.doctypes.topLevelPaths").toString().trim();
                String inspDirectory = propVal + "/ASIO/" + inspCode;
                newDir(inspDirectory, inspCode);
                appDir(inspCode, "REAIINSPECTION");
                appDir(inspCode, "REAIFACT");
                appDir(inspCode, "REAIROUTE");
                for (int i = 0; i < countroute; i++) {
                    MboRemote routeMbo = routeSet.getMbo(i); // получаем конкретный маршрут
                    routeMbo.setValue("REAISTATUS", "ПЕРЕДАНО В РАБОТУ", 11L);// проставили статус в маршрут
                    String routeCode = routeMbo.getString("REAICODE");
                    String routeDirectory = inspDirectory + "/" + routeCode;
                    newDir(routeDirectory, routeCode);
                    appDir(routeCode, "REAIINSPECTION");
                    appDir(routeCode, "REAIFACT");
                    appDir(routeCode, "REAIROUTE");
                }
                this.save();
                this.fireStructureChangedEvent();
                this.refreshTable();
                this.sessionContext.queueRefreshEvent();
                return 1;
            } else {
                Utility.showMessageBox(this.sessionContext.getCurrentEvent(), "REAI", "faultroutedata", null);
                return 1;
            }
        }
    }

    public int REAICOMPLETED()
            throws MXException, RemoteException {
        WebClientEvent event = clientSession.getCurrentEvent();
        int msgRet = event.getMessageReturn();
        if (msgRet < 0) {
            throw new MXApplicationException("REAI", "complete");
        } else if (msgRet == WebClientRuntime.MSG_BTNYES) {
            DataBean appBean = this.app.getAppBean();
            MboRemote inspectionMbo = appBean.getMbo();
            Date currentdate = new Date();
            ReaiRouteSet routeSet = (ReaiRouteSet) inspectionMbo.getMboSet("REAIROUTE");
            int countroute = routeSet.count();
            for (int i = 0; i < countroute; i++) {
                MboRemote routeMbo = routeSet.getMbo(i);
                MboSetRemote factMboSet = routeMbo.getMboSet("REAIFACT");
                int counFact = factMboSet.count();

                if (counFact == 0) {
                    inspectionMbo.setValue("REAISTATUS", "ЗАВЕРШЕНО", 11L);
                    routeMbo.setValue("REAISTATUS", "ЗАВЕРШЕНО", 11L);
                    inspectionMbo.setValue("REAICOMPLETEDATE", currentdate, 11L);
                    routeMbo.setValue("REAICOMPLETEDATE", currentdate, 11L);
                }
                else if (counFact!=0){

                    for (int j = 0; j < counFact; j++) {

                        MboRemote factMbo = factMboSet.getMbo(j);
                        String statusFact = factMbo.getString("REAISTATUS");

                        if (!statusFact.equals("СОГЛАСОВАНО")) {
                            Utility.showMessageBox(this.sessionContext.getCurrentEvent(), "REAI", "СompleteInsp", null);
                            break;
                        } else {
                            inspectionMbo.setValue("REAISTATUS", "ЗАВЕРШЕНО", 11L);
                            routeMbo.setValue("REAISTATUS", "ЗАВЕРШЕНО", 11L);
                            factMbo.setValue("REAISTATUS", "ЗАВЕРШЕНО", 11L);
                            inspectionMbo.setValue("REAICOMPLETEDATE", currentdate, 11L);
                            routeMbo.setValue("REAICOMPLETEDATE", currentdate, 11L);
                            factMbo.setValue("REAICOMPLETEDATE", currentdate, 11L);
                        }

                    }
                }
            }
            this.save();
            this.fireStructureChangedEvent();
            this.refreshTable();
            this.sessionContext.queueRefreshEvent();
        }
        return 1;
    }

    public int REAIDELETE()
            throws MXException, RemoteException {
        WebClientEvent event = clientSession.getCurrentEvent();
        int msgRet = event.getMessageReturn();
        if (msgRet < 0) {
            throw new MXApplicationException("REAI", "delete");
        } else if (msgRet == WebClientRuntime.MSG_BTNYES) {
            DataBean appBean = this.app.getAppBean();
            MboRemote inspectionMbo = appBean.getMbo();
            Date currentdate = new Date();
            inspectionMbo.setValue("REAISTATUS", "УДАЛЕНО", 11L);
            inspectionMbo.setValue("REAIDELETEDATE", currentdate, 11L);
            ReaiRouteSet routeSet = (ReaiRouteSet) inspectionMbo.getMboSet("REAIROUTE");
            int countroute = routeSet.count();
            for (int i = 0; i < countroute; i++) {
                MboRemote routeMbo = routeSet.getMbo(i);
                routeMbo.setValue("REAISTATUS", "УДАЛЕНО", 11L);
                routeMbo.setValue("REAIDELETEDATE", currentdate, 11L);
                MboSetRemote factSetMbo = routeMbo.getMboSet("REAIFACT");
                int countfact = factSetMbo.count();

                for (int j = 0; j < countfact; j++) {
                    MboRemote factMbo = factSetMbo.getMbo(j);
                    String factStatus = factMbo.getString("REAISTATUS");


                    if (factStatus.equals("УДАЛЕНО")) break;

                    else
                        factMbo.setValue("REAISTATUS", "УДАЛЕНО", 11L);
                        factMbo.setValue("REAIDELETEDATE", currentdate, 11L);
                }
            }
            this.save();
            this.fireStructureChangedEvent();
            this.refreshTable();
            this.sessionContext.queueRefreshEvent();
            return 1;
        }
        return 1;
    }

    public static synchronized void newDir(String Dir, String CODE)
            throws RemoteException, MXException {
        File theDir = null;
        theDir = new File(Dir);
        if (!theDir.exists()) {
            theDir.mkdir();
        }
        DoctypesSetRemote dirs = (DoctypesSetRemote) MXServer.getMXServer().getMboSet("DOCTYPES", MXServer.getMXServer().getSystemUserInfo());
        Boolean checkdir = Boolean.valueOf(true);
        if (dirs.count() > 0) {
            checkdir = Boolean.valueOf(false);
            for (int i = 0; i < dirs.count(); i++) {
                if (Objects.equals(dirs.getMbo(i).getString("DOCTYPE"), CODE)) {
                    checkdir = Boolean.valueOf(true);
                }
            }
        }
        if (!checkdir.booleanValue()) {
            DoctypesRemote dir = (DoctypesRemote) dirs.add();
            dir.setValue("DOCTYPE", CODE, 11L);
            dir.setValue("DESCRIPTION", CODE, 11L);
            dir.setValue("DEFAULTFILEPATH", Dir, 11L);
            dirs.save();
        }
    }

    public static synchronized void appDir(String CODE, String APP)
            throws RemoteException, MXException {
        AppDoctypeSetRemote dirs = (AppDoctypeSetRemote) MXServer.getMXServer().getMboSet("APPDOCTYPE", MXServer.getMXServer().getSystemUserInfo());
        Boolean checkdir = Boolean.valueOf(true);
        if (dirs.count() > 0) {
            checkdir = Boolean.valueOf(false);
            for (int i = 0; i < dirs.count(); i++) {
                if ((Objects.equals(dirs.getMbo(i).getString("DOCTYPE"), CODE)) && (Objects.equals(dirs.getMbo(i).getString("APP"), APP))) {
                    checkdir = Boolean.valueOf(true);
                }
            }
        }
        if (!checkdir.booleanValue()) {
            AppDoctypeRemote newappdir = (AppDoctypeRemote) dirs.add();
            newappdir.setValue("DOCTYPE", CODE, 11L);
            newappdir.setValue("APP", APP, 11L);
            dirs.save();
        }
    }

    public int REAIFACTSOGL() throws MXException, RemoteException {
        WebClientEvent event = clientSession.getCurrentEvent();
        int msgRet = event.getMessageReturn();
        if (msgRet < 0) {
            throw new MXApplicationException("REAI", "inspsoglfact");
        }
        else if (msgRet == WebClientRuntime.MSG_BTNYES) {
            DataBean appBean = this.app.getAppBean();
            MboRemote inspMbo = appBean.getMbo();// получили запись инспекции
            MboSetRemote routeSetMbo = inspMbo.getMboSet("REAIROUTEREADYFINISH");
            int counroute = routeSetMbo.count();
            for (int i = 0; i < counroute; i++) {

                MboRemote routeMbo = routeSetMbo.getMbo(i); // получили из выборки конкретную запись
                MboSetRemote factSetMbo = routeMbo.getMboSet("REAIFACTCHERN");
                int countfact = factSetMbo.count();

                for (int j = 0; j < countfact; j++) {

                    MboRemote factMbo = factSetMbo.getMbo(j);
                    factMbo.setValue("REAISTATUS", "СОГЛАСОВАНО", 11L);
                }
            }
            this.save();
            this.fireStructureChangedEvent();
            this.refreshTable();
            this.sessionContext.queueRefreshEvent();
            return 1;
        }
        return 1;
    }
}